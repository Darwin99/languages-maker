package com.lang.automata;

import java.util.HashSet;
import java.util.Set;

public class StateSet extends HashSet<State> {

	private static final long serialVersionUID = 1L;

//	/**
//	 * Map memoire lors d'un determinisation d'un automate
//	 */
//	protected HashMap<StateSet, State> mapDeterminise;

	public StateSet() {
		super();
//		mapDeterminise = null;
	}

	/**
	 * The set of state following the state in parameter
	 * 
	 * @param c The letter of transition
	 * @return the set of states reachable by the letter
	 */
	public StateSet succ(char c) {
		StateSet a = new StateSet();
		for (State etat : this) {
			StateSet tmp = etat.succ(c);
			a.addAll(tmp);
		}
		return a;
	}

	/**
	 * Test if the set of state contain a final state
	 * 
	 * @return Result of check
	 */
	public boolean containsTerminal() {
		for (State state : this) {
			if (state.isTerm())
				return true;
		}
		return false;
	}

	/**
	 * Test if the state set accepts the subword starting at position i
	 * 
	 * @param s word to be check
	 * @param i start position
	 * @return result of test
	 */
	public boolean accept(String s, int i) {
		if (i == s.length())
			return this.containsTerminal();
		if (this.succ(s.charAt(i)) != null) {
			return this.succ(s.charAt(i)).accept(s, ++i);
		}
		return false;
	}

	/**
	 * Get the alphabet of the state
	 * 
	 * @return Set of letter of transition
	 */
	public Set<Character> alphabet() {
		Set<Character> a = new HashSet<Character>();
		for (State etat : this) {
			for (Character c : etat.alphabet()) {
				a.add(c);
			}
		}
		return a;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		} else {
			final StateSet other = (StateSet) obj;
			if (this.isEmpty() && other.isEmpty())
				return true;
			for (State etat : this) {
				if (other.getEtat(etat.hashCode()) == null)
					return false;
			}

			for (State etat : other) {
				if (this.getEtat(etat.hashCode()) == null)
					return false;
			}
			return true;
		}
	}

	@Override
	public String toString() {
		String res = "";
		res += this.size() + " Etats\n";
		for (State etat : this) {
			res += "\n" + etat.toString();
		}
		return res;
	}


	/**
	 * Get the state with the id as a parameter, null if it is not there
	 * 
	 * @param id The id of the state to search
	 * @return The state found in null
	 */
	public State getEtat(int id) {
		for (State state : this) {
			if (state.hashCode() == id) {
				return state;
			}
		}
		return null;
	}

//	/**
//	 * Memorisation du mappage de la determinisation
//	 * 
//	 * @param e mappage utilisé
//	 */
//	public void setMapDeterminise(HashMap<StateSet, State> e) {
//		this.mapDeterminise = e;
//	}

}
