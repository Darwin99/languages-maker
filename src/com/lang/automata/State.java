package com.lang.automata;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class State {

	HashMap<Character, StateSet> transitions;

	boolean isInitial;

	boolean isFinal;

	int stateId;

	public State() {
		this.transitions = new HashMap<Character, StateSet>();
	}

	/**
	 * Initialise l'etat selon l'id et initial/acceptant
	 * 
	 * @param init vrai si initial
	 * @param term vrai si acceptant
	 * @param id   identifiant
	 */
	public State(boolean init, boolean term, int id) {
		this.transitions = new HashMap<Character, StateSet>();
		this.isInitial = init;
		this.isFinal = term;
		this.stateId = id;
	}

	/**
	 * Get all the successors of the transition by the letter in parameter
	 * 
	 * @param c The letter of the transition
	 * @return a set of states of successor states
	 */
	public StateSet succ(char c) {
		if (transitions.containsKey(c)) {
			return transitions.get(c);
		}
		return new StateSet();
	}

	/**
	 * Get all successors of all transitions
	 * 
	 * @return a set of states of successor states
	 */
	public StateSet succ() {
		if (!transitions.isEmpty()) {
			StateSet tmp = new StateSet();
			for (StateSet states : transitions.values()) {
				tmp.addAll(states);
			}
			return tmp;
		}
		return new StateSet();
	}

	/**
	 * Add a transition
	 * 
	 * @param c Letter of transition
	 * @param e state reached by the transition
	 */
	public void addTransition(char c, State e) {
		if (transitions.containsKey(c)) {
			StateSet tmp = transitions.get(c);
			tmp.add(e);
		} else {
			StateSet tmp = new StateSet();
			tmp.add(e);
			transitions.put(c, tmp);
		}
	}

	/**
	 * Get the alphabet of the state
	 * 
	 * @return Set of letter of transition
	 */
	public Set<Character> alphabet() {
		return transitions.keySet();
	}

	public boolean isInit() {
		return isInitial;
	}

	public boolean isTerm() {
		return isFinal;
	}

	public void setInit(boolean init) {
		this.isInitial = init;
	}

	public void setTerm(boolean term) {
		this.isFinal = term;
	}

	public HashMap<Character, StateSet> getTransitions() {
		return transitions;
	}

	@Override
	public int hashCode() {
		return stateId;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		} else {
			final State other = (State) obj;
			return (stateId == other.stateId);
		}
	}

	@Override
	public String toString() {
		String res = "";
		res += stateId + "";
		res += (isInitial) ? " initial" : "";
		res += (isFinal) ? " terminal" : "";
		res += "\n";

		for (Map.Entry<Character, StateSet> entry : transitions.entrySet()) {
			res += entry.getKey().charValue();
			for (State etat : entry.getValue()) {
				res += " " + etat.stateId;
			}
			res += "\n";
		}
		return res;
	}

}
