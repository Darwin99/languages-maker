package com.lang.automata;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import com.lang.tree.Leaf;
import com.lang.tree.Tree;

public class Automaton extends StateSet {

	private static final long serialVersionUID = 1989755685275535148L;

	private StateSet initials;

	public Automaton() {
		super();
		initials = new StateSet();
	}

	/**
	 * Automaton from a tree of a regular expression
	 * 
	 * @param The Tree containing the regular expression
	 */
	public Automaton(Tree arbre) {
		this();
		this.CreateAutomateFromArbre(arbre);
	}

	/**
	 * Add the state passed to parameter in the automaton
	 * 
	 * @param e Sate to be add
	 * @return boolean
	 */
	public boolean addStateToAutomaton(State e) {
		if (!this.add(e))
			return false;
		if (e.isInit())
			initials.add(e);
		return true;
	}

	/**
	 * Add the state passed in parameter and all his accessible state to automaton
	 * 
	 * @param e State to be add and where we can get all his accessible state
	 * @return Succes of all addition
	 */
	public boolean addStateAndHisAccessibleStateToAutomaton(State e) {
		if (addStateToAutomaton(e)) {
			StateSet succ = e.succ();
			if (succ != null) {
				for (State state : e.succ())
					addStateAndHisAccessibleStateToAutomaton(state);
			}
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Test id the automaton is deterministic
	 * 
	 * @return Result of test
	 */
	public boolean isDeterministic() {
		if (this.isEmpty())
			return true;

		if (this.initials.size() > 1)
			return false;

		for (State e : this) {
			for (Character a : e.getTransitions().keySet()) {
				if (e.succ(a).size() > 1) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Test if the word is accepting by the automaton
	 * 
	 * @param s the tested word
	 * @return The result of Test
	 */
	public boolean accept(String s) {
		return initials.accept(s, 0);
	}

	/**
	 * Create the automaton of the Tree representing th eregular expression
	 * 
	 * @param tree The Tree of regular expression
	 */
	private void CreateAutomateFromArbre(Tree tree) {
		initials.clear();
		this.clear();

		int comptId = 0;
		HashMap<Leaf, State> map = new HashMap<Leaf, State>();
		Map<Leaf, Set<Leaf>> succ = tree.succ();
		Stack<Leaf> pile = new Stack<Leaf>();

		State initialState = new State(true, tree.containsEmptyWord, comptId++);
		Leaf initialLeaf = new Leaf(Tree.EMPTY_WORD);
		this.addStateToAutomaton(initialState);
		succ.put(initialLeaf, tree.firsts);
		map.put(initialLeaf, initialState);
		pile.push(initialLeaf);

		while (!pile.isEmpty()) {
			Leaf currentLeaf = pile.pop();
			if (succ.get(currentLeaf) != null) {
				for (Leaf successiveLeaf : succ.get(currentLeaf)) {
					State currentState = map.get(successiveLeaf);
					if (currentState == null) {
						currentState = new State(false, tree.lasts.contains(successiveLeaf), comptId++);
						this.addStateToAutomaton(currentState);
						map.put(successiveLeaf, currentState);
						pile.push(successiveLeaf);
					}
					map.get(currentLeaf).addTransition(successiveLeaf.nodeSymbol, currentState);
				}
			}
		}
	}

	/**
	 * Cree l'automate determinise
	 * 
	 * @return l'automate determinise
	 */
//	public Automaton determinise() {
//		if (this.isDeterministic())
//			return this;
//		Automaton det = new Automaton();
//		HashMap<StateSet, State> map = new HashMap<StateSet, State>();
//		Stack<StateSet> pile = new Stack<StateSet>();
//		Set<Character> alphabet = this.alphabet();
//		ArrayList<StateSet> listEnsEtat = new ArrayList<StateSet>();
//		int etatCompteur = 0;
//
//		State initEtat = new State(true, false, etatCompteur++);
//		det.addStateToAutomaton(initEtat);
//		StateSet init = new StateSet();
//		for (State etat : initials)
//			init.add(etat);
//		map.put(init, initEtat);
//		pile.push(init);
//		listEnsEtat.add(init);
//
//		while (!pile.empty()) {
//			StateSet ensPop = pile.pop();
//			// Recupere etat lie a l'ensemble d'etat pris dans la pile
//			State etatLie = map.get(ensPop);
//			// Parcours de l'alphabet de l'automate
//			for (Character a : alphabet) {
//				StateSet etatsLieSuccA = new StateSet();
//				// Parcours de l'ensemble d'etats pris dans la pile
//				for (State etatPop : ensPop) {
//					// Recupere les etats succeceurs a la lettre courante
//					StateSet etatsSucc = etatPop.succ(a.charValue());
//					if (etatsSucc != null) {
//						// Ajoute les etats successeurs a un ensemble des successeurs de la lettre
//						// courante
//						for (State tmp : etatsSucc)
//							etatsLieSuccA.add(tmp);
//					}
//				}
//
//				// Si on a trouver des successeurs pour la lettre courante
//				if (!etatsLieSuccA.isEmpty()) {
//					// Cherche si on a pas deja creer un ensemble d'etat correspondant
//					// aux successeurs de la lettre courante
////					boolean existeDeja = false;
//					StateSet refMemeEnsEtat = null;
//					for (StateSet listEtat : listEnsEtat) {
//						if (listEtat.egale(etatsLieSuccA)) {
//							refMemeEnsEtat = listEtat;
//							break;
//						}
//					}
//
//					// Si l'ensemble des successeurs de la lettre courante existe pas deja
//					if (refMemeEnsEtat == null) {
//						State nouvelEtat = new State(false, etatsLieSuccA.contientTerminal(), etatCompteur++);
//						det.addStateToAutomaton(nouvelEtat);
//						map.put(etatsLieSuccA, nouvelEtat);
//						pile.push(etatsLieSuccA);
//						listEnsEtat.add(etatsLieSuccA);
//						etatLie.addTransition(a.charValue(), nouvelEtat);
//					} else {
//						etatLie.addTransition(a.charValue(), map.get(refMemeEnsEtat));
//					}
//				}
//			}
//		}
//		det.setMapDeterminise(map);
//
//		return det;
//	}

	@Override
	public String toString() {
		return super.toString();
	}
}
