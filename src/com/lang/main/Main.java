package com.lang.main;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import com.lang.automata.Automaton;
import com.lang.tree.Tree;
import com.lang.utilities.InfixToPostfix;

public class Main {

	public static void main(String[] args) {

		System.out.println("Enter a regular expression with dot(.) like concat symbole !");
		Scanner sc = new Scanner(System.in);
		String regularExpression = sc.nextLine();
		
		//Transform regular expression to postfix form
		String exp = InfixToPostfix.infixToPostfix(regularExpression);
		
		Tree arbre = Tree.PostfixToArbre(exp);
		
		//Create automate from regular Try
		Automaton automate = new Automaton(arbre);
//		automate.determinise();
		
		
		String response = "";

		do {
			System.out.println("Enter a word to test your language !");
			String word = sc.nextLine();

			boolean result = check(automate, word);

			if (result) {
				System.out.println("Accept");
			} else
				System.out.println("Reject");

			System.out.println("\nPress o to enter another word, other letter to quit");
			response = sc.nextLine();

		} while (response.equalsIgnoreCase("o"));
		sc.close();
	}

	public static boolean check(Automaton automate, String chaine) {
		return automate.accept(chaine);
	}

	public static void statistics(String chaine) {
		int wordLength = chaine.length();

		HashMap<Character, Integer> occurence = new HashMap<>();

		for (int i = 0; i < chaine.length(); i++) {
			char c = chaine.charAt(i);
			int nbre = count(chaine, c);
			occurence.put(c, nbre);
		}

		System.out.println("\n##################################################################");
		System.out.println("The word \"" + chaine + "\" have " + wordLength + " characters\n");
		for (Map.Entry<Character, Integer> occ : occurence.entrySet()) {
			System.out.println("The number of " + occ.getKey() + " is " + occ.getValue());
		}
		System.out.println("\n##################################################################");

	}

	public static int count(String word, char character) {
		int cpt = 0;
		for (int i = 0; i < word.length(); i++) {
			if (word.charAt(i) == (int) character)
				cpt++;
		}
		return cpt;
	}

}
