package com.lang.tree;

import java.util.Map;
import java.util.Set;
import java.util.Stack;

public abstract class Tree {

	public static final char CONCAT_SYMBOL = '.';
	public static final char OR_SYMBOL = '+';
	public static final char EMPTY_WORD = '1';

	/**
	 * Count the number of instanciation of a node
	 */
	public static int ID_COMPT = 0;

	public int nodeId;

	public char nodeSymbol;

	public boolean containsEmptyWord;

	public Set<Leaf> firsts;

	public Set<Leaf> lasts;

	/**
	 * Successors (transition) of the node subtree
	 * 
	 * @return The HashMap of the successors
	 */
	public abstract Map<Leaf, Set<Leaf>> succ();

	/**
	 * Test if the tree is equal to the one passed in argument
	 * 
	 * @param a Tree to be compared
	 * @return The result of test
	 */
	public abstract boolean isEquals(Tree a);

	/**
	 * Simplification of blank word sheets
	 * 
	 * @return The Tree without blank word
	 */
	public abstract Tree simplification();

	/**
	 * Get the alphabet of the Tree
	 * 
	 * @return Set of letter
	 */
	public abstract Set<Character> alphabet();

	/**
	 * Converting a regular expression written in postfix read to tree
	 * 
	 * @param expr regular expression
	 * @return The Tree of regular expression
	 */
	public static Tree PostfixToArbre(String expr) {
		Stack<Tree> pile = new Stack<Tree>();

		for (int i = 0; i < expr.length(); i++) {
			char valeur = expr.charAt(i);
			if (valeur == '*') {
				Tree element = pile.pop();
				Unary unaire = new Unary(element);
				pile.push(unaire);
			} else if (valeur == CONCAT_SYMBOL || valeur == OR_SYMBOL) {
				Tree element1 = pile.pop();
				Tree element2 = pile.pop();
				Binary binaire = new Binary(valeur, element2, element1);
				pile.push(binaire);
			} else {
				Leaf f = new Leaf(valeur);
				pile.push(f);
			}
		}

		return pile.pop();
	}

}
