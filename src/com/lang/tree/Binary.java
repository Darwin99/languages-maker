package com.lang.tree;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Binary extends Tree {

	public Tree leftTree;

	public Tree rightTree;

	public Binary(char symbole, Tree leftTree, Tree rightTree) {
		this.nodeId = Tree.ID_COMPT++;
		this.nodeSymbol = symbole;
		this.leftTree = leftTree;
		this.rightTree = rightTree;
		this.firsts = new HashSet<Leaf>();
		this.lasts = new HashSet<Leaf>();

		if (symbole == Tree.CONCAT_SYMBOL) {
			this.containsEmptyWord = leftTree.containsEmptyWord && rightTree.containsEmptyWord;

			this.firsts.addAll(leftTree.firsts);
			if (leftTree.containsEmptyWord)
				this.firsts.addAll(rightTree.firsts);

			this.lasts.addAll(rightTree.lasts);
			if (rightTree.containsEmptyWord)
				this.lasts.addAll(leftTree.lasts);
		} else {
			this.containsEmptyWord = leftTree.containsEmptyWord || rightTree.containsEmptyWord;

			this.firsts.addAll(leftTree.firsts);
			this.firsts.addAll(rightTree.firsts);

			this.lasts.addAll(leftTree.lasts);
			this.lasts.addAll(rightTree.lasts);
		}
	}

	/**
	 * Test if the tree is equal to the one passed in argument
	 * 
	 * @param a Tree to be compared
	 * @return The result of test
	 */
	public boolean isEquals(Tree a) {
		if (nodeSymbol == a.nodeSymbol) {
			Binary k = (Binary) a;
			return leftTree.isEquals(k.leftTree) && rightTree.isEquals(k.rightTree);
		}
		return false;
	}

	/**
	 * Simplification of blank word sheets
	 * 
	 * @return The Tree without blank word
	 */
	public Tree simplification() {
		leftTree = leftTree.simplification();
		rightTree = rightTree.simplification();
		if (leftTree.nodeSymbol == Tree.EMPTY_WORD && rightTree.nodeSymbol == Tree.EMPTY_WORD)
			return new Leaf(Tree.EMPTY_WORD);
		if (leftTree.nodeSymbol == Tree.EMPTY_WORD)
			return rightTree;
		if (rightTree.nodeSymbol == Tree.EMPTY_WORD)
			return leftTree;
		return this;
	}

	/**
	 * Get the alphabet of the Tree
	 * 
	 * @return Set of letter
	 */
	public Set<Character> alphabet() {
		Set<Character> a = new HashSet<Character>();
		a.addAll(leftTree.alphabet());
		a.addAll(rightTree.alphabet());
		return a;
	}

	public Map<Leaf, Set<Leaf>> succ() {
		HashMap<Leaf, Set<Leaf>> map = new HashMap<Leaf, Set<Leaf>>();
		map.putAll(leftTree.succ());
		map.putAll(rightTree.succ());
		if (nodeSymbol == Tree.CONCAT_SYMBOL) {
			for (Leaf g : leftTree.lasts) {
				for (Leaf d : rightTree.firsts) {
					if (map.containsKey(g)) {
						Set<Leaf> tmp = map.get(g);
						tmp.add(d);
					} else {
						Set<Leaf> tmp = new HashSet<Leaf>();
						tmp.add(d);
						map.put(g, tmp);
					}
				}
			}
		}
		return map;
	}

	@Override
	public String toString() {
		if (nodeSymbol == Tree.CONCAT_SYMBOL)
			return "[" + leftTree.toString() + nodeSymbol + rightTree.toString() + "]";
		else
			return "(" + leftTree.toString() + nodeSymbol + rightTree.toString() + ")";
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		} else {
			final Binary other = (Binary) obj;
			return (nodeSymbol == other.nodeSymbol) && leftTree.equals(other.leftTree)
					&& rightTree.equals(other.rightTree);
		}
	}

	@Override
	public int hashCode() {
		if (nodeSymbol == Tree.CONCAT_SYMBOL) {
			return (int) (leftTree.hashCode() * rightTree.hashCode());
		} else {
			return (int) (leftTree.hashCode() + rightTree.hashCode());
		}

	}

}
