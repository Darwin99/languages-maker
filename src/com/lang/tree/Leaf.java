package com.lang.tree;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Leaf extends Tree {

	public Leaf(char symbol) {
		this.nodeId = Tree.ID_COMPT++;
		this.nodeSymbol = symbol;
		this.containsEmptyWord = symbol == Tree.EMPTY_WORD;
		this.firsts = new HashSet<Leaf>();
		if (!this.containsEmptyWord)
			this.firsts.add(this);
		this.lasts = new HashSet<Leaf>();
		if (!this.containsEmptyWord)
			this.lasts.add(this);
	}

	public boolean isEquals(Tree a) {
		return nodeSymbol == a.nodeSymbol;
	}

	public Tree simplification() {
		return this;
	}

	public Set<Character> alphabet() {
		Set<Character> a = new HashSet<Character>();
		if (nodeSymbol != Tree.EMPTY_WORD)
			a.add(nodeSymbol);
		return a;
	}

	public Map<Leaf, Set<Leaf>> succ() {
		return new HashMap<Leaf, Set<Leaf>>();
	}

	@Override
	public String toString() {
		return "" + nodeSymbol;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		} else {
			final Leaf other = (Leaf) obj;
			return (nodeId == other.nodeId);
		}
	}

	@Override
	public int hashCode() {
		return (int) nodeId;
	}

}
