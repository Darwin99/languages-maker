package com.lang.tree;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Unary extends Tree {
	
	public Tree son;

	
	public Unary() {
		this.nodeId = Tree.ID_COMPT++;
		this.nodeSymbol = '*';
		this.son = null;
	}

	public Unary(Tree son) {
		this();
		this.son = son;
		this.containsEmptyWord = true;
		this.firsts = new HashSet<Leaf>();
		this.firsts.addAll(son.firsts);
		this.lasts = new HashSet<Leaf>();
		this.lasts.addAll(son.lasts);
	}


	/**
	 * Check if the Tree is equal to the one passed in parameter
	 * 
	 * @param The Tree to be compared
	 * @return The result of checking
	 */
	public boolean isEquals(Tree a) {
		if (nodeSymbol == a.nodeSymbol) {
			Unary k = (Unary) a;
			return son.isEquals(k.son);
		}
		return false;
	}

	/**
	 * Simplification of blank word sheets
	 * 
	 * @return The Tree without blank word
	 */
	public Tree simplification() {
		son = son.simplification();
		if (son.nodeSymbol == Tree.EMPTY_WORD)
			return new Leaf(Tree.EMPTY_WORD);
		return this;
	}

	/**
	 * Get the alphabet of the Tree
	 * 
	 * @return Set of letter
	 */
	public Set<Character> alphabet() {
		return son.alphabet();
	}

	public Map<Leaf, Set<Leaf>> succ() {
		HashMap<Leaf, Set<Leaf>> map = new HashMap<Leaf, Set<Leaf>>();
		map.putAll(son.succ());
		for (Leaf lastLeaf : lasts) {
			for (Leaf firstLeaf : firsts) {
				if (map.containsKey(lastLeaf)) {
					map.get(lastLeaf).add(firstLeaf);
				} else {
					Set<Leaf> tmp = new HashSet<Leaf>();
					tmp.add(firstLeaf);
					map.put(lastLeaf, tmp);
				}
			}
		}
		return map;
	}

	@Override
	public String toString() {
		return "{" + son.toString() + "}" + nodeSymbol;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		} else {
			final Unary other = (Unary) obj;
			return (nodeSymbol == other.nodeSymbol) && son.equals(other.son);
		}
	}

	@Override
	public int hashCode() {
		return (int) son.hashCode();
	}
}
